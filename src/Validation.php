<?php


class Validation
{
    /** @var Figure $figure */
    private $figure;

    public function __construct(Figure $figure)
    {
        $this->figure = $figure;
    }

    public function checkRotation(?Figure $prevFigure)
    {
        if (
            $prevFigure &&
            $prevFigure->getIsBlack() === $this->figure->getIsBlack()
        ) {
            throw new Exception("Ошибка очерёдности хода");
        }
    }

    public function checkStep($figures, $xFrom, $yFrom, $xTo, $yTo)
    {
        if ($this->figure instanceof Pawn) {
            if (ord($xFrom) > (ord($xTo) + 1) && ord($xFrom) < (ord($xTo) - 1)){
                throw new Exception("Ошибка хода пешки");
            }

            $isNotFirstStep = !in_array($this->figure, History::getInstance()->getHistory(), true);
            $isBlack        = $this->figure->getIsBlack();
            if (
                $yTo - ($yFrom + 1 * (1 - (int)$isBlack * 2)) &&
                $yTo - ($yFrom + (1 + (int)$isNotFirstStep)* (1 - (int)$isBlack * 2))
            )
            {
                throw new Exception("Ошибка хода пешки");
            }

            if (
                $xFrom === $xTo &&
                (
                    isset($figures[$xTo][$yTo]) ||
                    (
                        $yTo - $yFrom > 1 &&
                        isset($figures[$xTo][($yTo - 1 * (1 - (int)$isBlack * 2))])
                    )
                )
            ) {
                throw new Exception("Ошибка хода пешки");
            }

            if ($xFrom !== $xTo && !isset($figures[$xTo][$yTo])) {
                throw new Exception("Ошибка хода пешки");
            }

            if ($xFrom !== $xTo && $yTo - ($yFrom + 1 * (1 - (int)$isBlack * 2))) {
                throw new Exception("Ошибка хода пешки");
            }
        }
    }
}