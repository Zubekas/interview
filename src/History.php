<?php


class History
{
    /**
     * @var History
     */
    private static $instance;

    private static $increment;
    private static $historyGame;

    public function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }
    private function __construct()
    {
        self::$increment        = 0;
        self::$historyGame      = [];
    }

    public function step(Figure $currentFigure)
    {
        self::$historyGame[self::$increment++] = $currentFigure;
    }

    public function getPreviousFigure()
    {
        if (self::$increment < 1) {
            return null;
        }
        return self::$historyGame[self::$increment - 1];
    }

    public function getHistory()
    {
        return self::$historyGame;
    }
}